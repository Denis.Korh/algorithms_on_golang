/*
Визуализация графа:
a -- b -- c -- f
     |         |
     d -- i -- j

Сам граф:
graph := map[string][]string{
    "a": {"b"},
    "b": {"a", "c", "d"},
    "c": {"b", "f"},
    "d": {"b", "i"},
    "f": {"c", "j"},
    "i": {"d", "j"},
    "j": {"i", "f"},
}
*/
package breadthfirstsearch

func BreadthFirstSearch(graph map[string][]string, start, destination string) (result int) {
	processed := map[string]bool{}
	processVertexRecursive(start, graph[start], destination, &graph, &result, &processed)
	return
}

func processVertexRecursive(
	start string,
	vertices []string,
	destination string,
	graph *map[string][]string,
	result *int,
	processed *map[string]bool,
) {
	*result++ // счётчик итераций
	nextLevelVertices := make([]string, 0) //очередь для соседей
	for _, v := range vertices {
		if v == start {
			continue // не движемся в обратном направлении
		}
		if (*processed)[v] {
			continue // не перебираем одну вершину два раза
		}
		(*processed)[v] = true
		if v == destination {
			return // мы дошли до целевой вершины
		} else {
			nextLevelVertices = append(nextLevelVertices, (*graph)[v]...)
		}
	}

	processVertexRecursive(start, nextLevelVertices, destination, graph, result, processed)
}