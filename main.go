//Здесь можно импортировать и протестировать один из алгоритмов.
package main

import (
	a "algorithms_on_golang/selection_sort"
	"fmt"
)

func main() {
	sl := []int{3, 7, 9, 36, 9, 1, 4, 56, 0, 0, 12435}
	fmt.Println(sl)
	a.SelectionSort(sl)
	fmt.Println(sl)
}
