package bubblesort

func BubbleSort(input []int) {
	SortRecursive(input, 0, false, len(input)-2, 0)
}

func SortRecursive(input []int, i int, shifted bool, preLastIndex, iterationsCount int) ([]int, int) {
	iterationsCount++ //счётчик итераций
	if i == 0 {
		shifted = false //сбрасываем флаг shifted после сброса индкса
	}

	if input[i] > input[i + 1] { //сравниваем два значения
		input[i], input[i + 1] = input[i + 1], input[i] //меняем значение, если левое - больше
		//обрабатываем предпоследний индекс:
		if preLastIndex == i {
			// если не было обмена значениями - меняем последние 2 значения и возвращаем результат
			if shifted == false {
				return input, iterationsCount
			}
		}
		shifted = true
	} else {
		//обрабатываем предпоследний индекс
		if preLastIndex == i {
			//если не было обмена значениями - меняем последние 2 значения и возвращаем результат
			if shifted == false {
				return input, iterationsCount
			}
		}
	}

	//обрабатываем предпоследний индекс вне зависимости от того, был ли обмен значениями
	if preLastIndex == i {
		i = 0 // если предпоследний - сбрасываем индекс
	} else {
		i++
	}

	//рекурсивный вызов
	return SortRecursive(input, i, shifted, preLastIndex, iterationsCount)
}