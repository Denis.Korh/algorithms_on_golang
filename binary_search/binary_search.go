package binarysearch

import "sort"

func BinarySearch(haystack []int, needle int) (needleIndex int, result bool) {
	sort.Ints(haystack)
	lowKey := 0
	highKey := len(haystack) -1
	if haystack[lowKey] > needle || haystack[highKey] < needle {
		return //искомого значения нет в haystack
	}

	for lowKey <= highKey {
		mid := (lowKey + highKey) / 2
		if haystack[mid] == needle {
			return mid, true //значение найдено
		}
		
		if haystack[mid] < needle { //отбрасываем половину
			lowKey = mid + 1
		} else {
			highKey = mid -1 
		}
	}

	return
}